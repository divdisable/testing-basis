const getFibonacci = require("./fibonacci");

describe("fibonacci", function () {
  it("Should return 0 for n = 0", () => {
    const result = getFibonacci(0);
    expect(result).toBe(0);
  });

  it("Should return 1 for n = 1", () => {
    const result = getFibonacci(1);
    expect(result).toBe(1);
  });

  it("Should return 0 for n < 0", () => {
    const result = getFibonacci(-1);
    expect(result).toBe(0);
  });

  it("Should return apropriate number for input", () => {
    // 0 1 1 2 3 5 8 13 21 34 55 87...
    const data = {
      "1": 1,
      "2": 1,
      "3": 2,
      "4": 3,
      "5": 5,
      "6": 8,
      "7": 13,
      "8": 21
    };

    for (const input in data) {
      expect(getFibonacci(Number(input))).toBe(data[input]);
    }
  });
});
