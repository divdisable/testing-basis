const startServer = require("./simpleTestApp");
const supertest = require("supertest");

const util = require("util");


describe("simpleTestApp", function () {
  // beforeEach(async () => {
  //   server = await startServer();
  // });

  // afterEach(async () => {
  //   await server.stop();
  // });

  it("Should return empty array on GET request", async () => {
    const server = await startServer();
    const res = await supertest("http://localhost:3000")
      .get("")
      .expect(200)
    const stopServer = util.promisify(server.stop.bind(server));
    await stopServer();
    expect(res.body).toHaveLength(0);
  });

  it("Should require title in body on POST", async () => {
    const server = await startServer();
    const res = await supertest("http://localhost:3000")
      .post("")
      .send({})
      .expect(400)
    const stopServer = util.promisify(server.stop.bind(server));
    await stopServer();
  });

  it("Should add task", async () => {
    const server = await startServer();
    // add task
    const res = await supertest("http://localhost:3000")
      .post("")
      .send({
        title: "My first task",
        description: "It's short description"
      })
      .expect(200);

    // get task
    const tasks = await supertest("http://localhost:3000")
      .get("")
      .expect(200);
    expect(tasks.body[0].title).toBe("My first task");

    const stopServer = util.promisify(server.stop.bind(server));
    await stopServer();
  });
});
