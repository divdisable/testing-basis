const setupServer = require("./simpleTestApp.js");

const startServer = async () => {
  await setupServer();
};

startServer();
