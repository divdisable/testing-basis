const findText = require("./findText");
const getFileContent = require("./getFileContent");

jest.mock("./getFileContent");

describe("findText", function () {
  it("Should return true if text is found in the file", async () => {
    getFileContent.mockResolvedValue("This is a sample Text");

    const result = await findText("samplePath", "sample Text");
    expect(result).toBe(true);
  })

  it("Should return false if text is not found in the file", async () => {
    getFileContent.mockResolvedValue("This is a sample Text");

    const result = await findText("samplePath", "not-existing Text");
    expect(result).toBe(false);
  })
});
